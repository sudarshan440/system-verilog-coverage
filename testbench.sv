// Code your testbench here
// or browse Examples
module top;
  bit a;
  bit b;
  
  covergroup first();
    feature_a : coverpoint a;
    feature_b : coverpoint b;
  endgroup
  
  initial begin
    first f;
    f = new();
    f.sample();
    
  end
endmodule : top